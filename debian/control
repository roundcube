Source: roundcube
Section: web
Priority: extra
Maintainer: Debian Roundcube Maintainers <pkg-roundcube-maintainers@lists.alioth.debian.org>
Uploaders: Vincent Bernat <bernat@debian.org>, Romain Beauxis <toots@rastageeks.org>
Build-Depends: cdbs, debhelper (>= 5), dh-buildinfo, po-debconf, yui-compressor
Homepage: http://www.roundcube.net/
Standards-Version: 3.9.3
Vcs-Git: git://anonscm.debian.org/pkg-roundcube/roundcube.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-roundcube/roundcube.git

Package: roundcube-core
Architecture: all
Depends: dbconfig-common, debconf | debconf-2.0, ucf,
 apache2 | lighttpd | httpd,
 php5, php5-mcrypt, php5-gd, php5-intl,
 php-mdb2 (>= 2.5.0), php-auth, php-net-smtp (>= 1.4.2),
 php-net-socket, php-mail-mime (>= 1.8.2), php5-pspell,
 tinymce (>= 3), libjs-jquery (>= 1.6.4), libjs-jquery-ui (>= 1.8),
 libmagic1,
 roundcube-mysql (= ${source:Version}) | roundcube-pgsql (= ${source:Version}),
 ${misc:Depends}
Suggests: php-auth-sasl (>= 1.0.3), php-crypt-gpg, roundcube-plugins
Conflicts: roundcube-plugins-extra (<= 0.6-20111030)
Description: skinnable AJAX based webmail solution for IMAP servers
 RoundCube Webmail is a browser-based multilingual IMAP client with an
 application-like user interface. It provides full functionality
 expected from an e-mail client, including MIME support, address book,
 folder manipulation and message filters.
 .
 The user interface is fully skinnable using XHTML and CSS 2.
 .
 This package provides the core code for roundcube. You should install
 it along with one of the roundcube database metapackages.

Package: roundcube
Architecture: all
Depends: roundcube-core (= ${source:Version}), ${misc:Depends}
Description: skinnable AJAX based webmail solution for IMAP servers - metapackage
 RoundCube Webmail is a browser-based multilingual IMAP client with an
 application-like user interface. It provides full functionality
 expected from an e-mail client, including MIME support, address book,
 folder manipulation and message filters.
 .
 The user interface is fully skinnable using XHTML and CSS 2.
 .
 This package will install a full roundcube application.

Package: roundcube-sqlite
Architecture: all
Depends: roundcube-mysql | roundcube-pgsql, ${misc:Depends}
Section: oldlibs
Description: transitional dummy package
 This package is a dummy transitional package. It can be safely removed.

Package: roundcube-mysql
Architecture: all
Depends: php-mdb2-driver-mysql (>= 1.5.0b2), mysql-client | virtual-mysql-client, ${misc:Depends}
Suggests: mysql-server
Description: metapackage providing MySQL dependencies for RoundCube
 This package provides MySQL dependencies for RoundCube Webmail, a
 skinnable AJAX based webmail solution for IMAP servers. Install this
 one if you want to use a MySQL database with RoundCube.

Package: roundcube-pgsql
Architecture: all
Depends: php-mdb2-driver-pgsql (>= 1.5.0b2), postgresql-client-8.1 | postgresql-client, ${misc:Depends}
Suggests: postgresql-server
Description: metapackage providing PostgreSQL dependencies for RoundCube
 This package provides PostgreSQL dependencies for RoundCube Webmail,
 a skinnable AJAX based webmail solution for IMAP servers. Install
 this one if you want to use a PostgreSQL database with RoundCube.

Package: roundcube-plugins
Architecture: all
Depends: roundcube-core (= ${source:Version}), ${misc:Depends}
Description: skinnable AJAX based webmail solution for IMAP servers - plugins
 RoundCube Webmail is a browser-based multilingual IMAP client with an
 application-like user interface. It provides full functionality
 expected from an e-mail client, including MIME support, address book,
 folder manipulation and message filters.
 .
 The user interface is fully skinnable using XHTML and CSS 2.
 .
 This package provides several plugins for Roundcube. They should be
 enabled in the configuration of Roundcube.
